all: install

install:
	CGO_ENABLED=0 go install gitlab.com/Cavegn/project-operator/cmd/project-operator

dep:
	dep ensure -v

