FROM alpine:3.8

ADD bin/project-operator /usr/local/bin/project-operator

RUN adduser -D project-operator
USER project-operator

CMD ["project-operator"]