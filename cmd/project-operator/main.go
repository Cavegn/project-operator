package main

import (
	"context"
	"github.com/operator-framework/operator-sdk/pkg/k8sclient"
	"k8s.io/api/authorization/v1"
	"runtime"
	"time"

	"github.com/operator-framework/operator-sdk/pkg/sdk"
	"github.com/operator-framework/operator-sdk/pkg/util/k8sutil"
	sdkVersion "github.com/operator-framework/operator-sdk/version"
	"gitlab.com/Cavegn/project-operator/pkg/stub"

	"github.com/sirupsen/logrus"
	_ "k8s.io/client-go/plugin/pkg/client/auth/gcp"
)

func printVersion() {
	logrus.Infof("Go Version: %s", runtime.Version())
	logrus.Infof("Go OS/Arch: %s/%s", runtime.GOOS, runtime.GOARCH)
	logrus.Infof("operator-sdk Version: %v", sdkVersion.Version)
}

func main() {
	// os.Setenv("OPERATOR_NAME", "project-operator")
	// os.Setenv("WATCH_NAMESPACE", "project-operator")
	// os.Setenv("KUBERNETES_CONFIG", "c:/Users/chris/.kube/config")

	//operatorName := os.Getenv("OPERATOR_NAME")
	//logPermissionAvailable("", "namespace", "create", operatorName)
	//logPermissionAvailable("", "secret", "create", operatorName)
	//logPermissionAvailable("", "serviceaccount", "create", operatorName)
	//logPermissionAvailable("rbac.authorization.k8s.io", "rolebinding", "create", operatorName)

	printVersion()

	sdk.ExposeMetricsPort()

	resource := "project.operator/v1alpha1"
	kind := "NamespacedProject"
	namespace, err := k8sutil.GetWatchNamespace()
	if err != nil {
		logrus.Fatalf("failed to get watch namespace: %v", err)
	}
	resyncPeriod := time.Duration(5) * time.Second
	logrus.Infof("Watching %s, %s, %s, %d", resource, kind, namespace, resyncPeriod)
	sdk.Watch(resource, kind, namespace, resyncPeriod)
	sdk.Handle(stub.NewHandler())
	sdk.Run(context.TODO())
}

//
// Logs if current user has permission to modify specified resource.
//
func logPermissionAvailable(group string, resource string, verb string, operatorName string) {
	client := k8sclient.GetKubeClient()
	review := v1.SelfSubjectAccessReview{
		Spec: v1.SelfSubjectAccessReviewSpec{
			ResourceAttributes: &v1.ResourceAttributes{
				Group:    group,
				Resource: resource,
				Verb:     verb,
			},
		},
	}
	result, err := client.AuthorizationV1().SelfSubjectAccessReviews().Create(&review)
	if err != nil {
		logrus.Printf("Error while checking permissions: %s", err)
	} else {
		reason := ""
		if result.Status.Reason != "" {
			reason = " (" + result.Status.Reason + ")"
		}
		logrus.Printf("Is Operator %s allowed to '%s' '%s' in Group '%s': %t%s",
			operatorName, verb, resource, group, result.Status.Allowed, reason)
	}
}
