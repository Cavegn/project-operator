package secret

import (
	"github.com/operator-framework/operator-sdk/pkg/sdk"
	"github.com/sirupsen/logrus"
	"gitlab.com/Cavegn/project-operator/pkg/constants"
	"k8s.io/api/core/v1"
	metav1 "k8s.io/apimachinery/pkg/apis/meta/v1"
)

func CreateOrUpdateSecrets(sourceNamespace string, destinationNamespace v1.Namespace, secretsToPreserve []string) error {

	sourceSecrets, err := getSecrets(sourceNamespace)
	if err != nil {
		logrus.Errorf("failed to load secrets from namespace %s: %v", sourceNamespace, err)
		return err
	}

	destinationSecrets, err := getSecrets(destinationNamespace.Name)
	if err != nil {
		logrus.Errorf("failed to load secrets from namespace %s: %v", destinationNamespace.Name, err)
		return err
	}

	for _, secretToPreserve := range secretsToPreserve {
		sourceSecret, ok := sourceSecrets[secretToPreserve]
		if !ok {
			logrus.Errorf("failed to preserve secret %s from namespace %s. reason: secret doesn't exist",
				secretToPreserve, sourceNamespace)
			return err
		}

		delete(sourceSecrets, secretToPreserve)

		destinationSecret, ok := destinationSecrets[secretToPreserve]
		delete(destinationSecrets, secretToPreserve)

		if ok {
			// Secret exist -> modify if allowed
			if controlledByOperator(destinationSecret) {
				destinationSecret.Data = sourceSecret.Data
				destinationSecret.StringData = sourceSecret.StringData

				err := sdk.Update(&destinationSecret)
				if err != nil {
					logrus.Errorf("failed to update secret %s in namespace %s: %v", secretToPreserve, destinationNamespace.Namespace, err)
					return err
				}
			}
		} else {
			// Secret doesn't exist -> create
			destinationSecret.Annotations = map[string]string{
				constants.CREATED_BY_OPERATOR: constants.CREATED_BY_OPERATOR_TRUE,
			}
			destinationSecret.APIVersion = sourceSecret.APIVersion
			destinationSecret.Data = sourceSecret.Data
			destinationSecret.Kind = sourceSecret.Kind
			destinationSecret.Name = sourceSecret.Name
			destinationSecret.Namespace = destinationNamespace.Name
			destinationSecret.StringData = sourceSecret.StringData
			destinationSecret.Type = sourceSecret.Type

			err := sdk.Create(&destinationSecret)
			if err != nil {
				logrus.Errorf("failed to create secret %s in namespace %s: %v",
					secretToPreserve, destinationNamespace.Namespace, err)
				return err
			}
		}
	}

	for _, destinationSecret := range destinationSecrets {
		// existing / not configured secret => delete if created by operator
		if controlledByOperator(destinationSecret) {
			err := sdk.Delete(&destinationSecret)
			if err != nil {
				logrus.Errorf("failed to delete secret %s in namespace %s: %v",
					destinationSecret.Name, destinationNamespace.Namespace, err)
				return err
			}
		}
	}

	return nil
}

// Returns true if Secret is controlled by Operator
// (Secret has Annotation constants.CREATED_BY_OPERATOR with value constants.CREATED_BY_OPERATOR_TRUE)
func controlledByOperator(secret v1.Secret) bool {
	value, ok := secret.Annotations[constants.CREATED_BY_OPERATOR]
	return ok && value == constants.CREATED_BY_OPERATOR_TRUE
}

// Returns a Map containing all the Secrets of given Namespace
// Key = Name of the Secret
// Value = Secret
func getSecrets(namespace string) (map[string]v1.Secret, error) {
	secretList := secretList()
	listOptions := &metav1.ListOptions{}
	err := sdk.List(namespace, secretList, sdk.WithListOptions(listOptions))
	if err != nil {
		logrus.Errorf("failed to list secrets in namespace %s: %v", namespace, err)
		return nil, err
	}

	secrets := make(map[string]v1.Secret)
	for _, secret := range secretList.Items {
		secrets[secret.Name] = secret
	}

	return secrets, nil
}

func secretList() *v1.SecretList {
	return &v1.SecretList{TypeMeta: metav1.TypeMeta{Kind: "Secret", APIVersion: "v1"}}
}
