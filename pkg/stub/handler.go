package stub

import (
	"context"
	"gitlab.com/Cavegn/project-operator/pkg/apis/project/v1alpha1"
	"gitlab.com/Cavegn/project-operator/pkg/constants"
	"gitlab.com/Cavegn/project-operator/pkg/stub/rbac"
	"gitlab.com/Cavegn/project-operator/pkg/stub/secret"
	corev1 "k8s.io/api/core/v1"
	"k8s.io/apimachinery/pkg/api/errors"

	"github.com/operator-framework/operator-sdk/pkg/sdk"
	"github.com/sirupsen/logrus"
	metav1 "k8s.io/apimachinery/pkg/apis/meta/v1"
)

func NewHandler() sdk.Handler {
	return &Handler{}
}

type Handler struct {
	// Fill me
}

func (h *Handler) Handle(ctx context.Context, event sdk.Event) error {
	switch o := event.Object.(type) {
	case *v1alpha1.NamespacedProject:

		namespace := corev1.Namespace{
			TypeMeta: metav1.TypeMeta{
				Kind:       "Namespace",
				APIVersion: "v1",
			},
			ObjectMeta: metav1.ObjectMeta{
				Name: o.Spec.ProjectNamespace,
			},
		}

		if event.Deleted {
			logrus.Printf("Project %s/%s deletedub", o.Spec.ProjectNamespace, o.Spec.ProjectNamespace)
			return deleteProjectNamespace(namespace)
		} else {
			logrus.Printf("Project %s/%s detected", o.Spec.ProjectNamespace, o.Spec.ProjectNamespace)
			return setupProjectNamespace(namespace, o)
		}
	}
	return nil
}

func setupProjectNamespace(namespace corev1.Namespace, namespacedProject *v1alpha1.NamespacedProject) error {
	err := createOrUpdateNamespace(namespace, namespacedProject)
	if err != nil {
		return err
	}

	secret.CreateOrUpdateSecrets(namespacedProject.Namespace, namespace, namespacedProject.Spec.PreserveSecrets)
	createOrUpdateServiceAccounts(namespace, namespacedProject.Spec.ServiceAccounts)

	rbac.CreateClusterRoleBindings(namespacedProject, namespace)

	return nil
}

func createOrUpdateNamespace(namespace corev1.Namespace, namespacedProject *v1alpha1.NamespacedProject) error {
	annotations := map[string]string{
		constants.CREATED_BY_OPERATOR: constants.CREATED_BY_OPERATOR_TRUE,
		constants.PROJECT_NAME:        namespacedProject.Spec.ProjectName,
		constants.PROJECT_OWNER:       namespacedProject.Spec.ProjectOwner,
	}

	namespace.Annotations = annotations

	err := sdk.Create(&namespace)
	if err != nil {
		if errors.IsAlreadyExists(err) {
			err := sdk.Update(&namespace)
			if err != nil {
				logrus.Errorf("failed to update namespace: %v", err)
				return err
			}
		} else {
			logrus.Errorf("failed to create namespace: %v", err)
			return err
		}
	}

	return nil
}

func createOrUpdateServiceAccounts(namespace corev1.Namespace, users []v1alpha1.User) error {
	serviceAccountList := serviceAccountList()
	listOptions := &metav1.ListOptions{}
	err := sdk.List(namespace.Name, serviceAccountList, sdk.WithListOptions(listOptions))
	if err != nil {
		logrus.Errorf("failed to list serviceAccounts: %v", err)
		return err
	}

	serviceAccounts := make(map[string]corev1.ServiceAccount)
	for _, serviceAccount := range serviceAccountList.Items {
		serviceAccounts[serviceAccount.Name] = serviceAccount
	}

	// Create or update given ServiceAccounts
	for _, serviceAccount := range users {
		serviceAccount := corev1.ServiceAccount{
			TypeMeta: metav1.TypeMeta{Kind: "ServiceAccount", APIVersion: "v1"},
			ObjectMeta: metav1.ObjectMeta{
				Name:      serviceAccount.Name,
				Namespace: namespace.Name,
				Annotations: map[string]string{
					constants.CREATED_BY_OPERATOR: constants.CREATED_BY_OPERATOR_TRUE,
				},
			},
		}

		existingServiceAccount, ok := serviceAccounts[serviceAccount.Name]
		if ok {
			delete(serviceAccounts, serviceAccount.Name)

			// Preserve existing secrets (otherwise, every x seconds a new token will be created
			serviceAccount.Secrets = existingServiceAccount.Secrets

			value, ok := existingServiceAccount.Annotations[constants.CREATED_BY_OPERATOR]
			if ok && value == constants.CREATED_BY_OPERATOR_TRUE {
				err := sdk.Update(&serviceAccount)
				if err != nil {
					logrus.Errorf("failed to update ServiceAccount %s: %v", serviceAccount.Name, err)
					return err
				}
			}
		} else {
			err := sdk.Create(&serviceAccount)
			if err != nil {
				logrus.Errorf("failed to create ServiceAccount %s: %v", serviceAccount.Name, err)
				return err
			}
		}
	}

	// Remove ServiceAccounts, which were created by operator and aren't configured any more
	for _, serviceAccount := range serviceAccounts {
		value, ok := serviceAccount.Annotations[constants.CREATED_BY_OPERATOR]
		if ok && value == constants.CREATED_BY_OPERATOR_TRUE {
			gracePeriodSeconds := int64(5)
			metav1DeleteOptions := &metav1.DeleteOptions{GracePeriodSeconds: &gracePeriodSeconds}
			err := sdk.Delete(&serviceAccount, sdk.WithDeleteOptions(metav1DeleteOptions))
			if err != nil {
				logrus.Errorf("failed to delete ServiceAccount %s/%s: %v", serviceAccount.Namespace, serviceAccount.Name, err)
				return err
			}
		}
	}

	return nil
}

func serviceAccountList() *corev1.ServiceAccountList {
	return &corev1.ServiceAccountList{TypeMeta: metav1.TypeMeta{Kind: "ServiceAccount", APIVersion: "v1"}}
}

func deleteProjectNamespace(namespace corev1.Namespace) error {
	gracePeriodSeconds := int64(5)
	metav1DeleteOptions := &metav1.DeleteOptions{GracePeriodSeconds: &gracePeriodSeconds}
	err := sdk.Delete(&namespace, sdk.WithDeleteOptions(metav1DeleteOptions))

	if err != nil && !errors.IsNotFound(err) {
		logrus.Errorf("failed to delete namespace: %v", err)
		return err
	}

	return nil
}
