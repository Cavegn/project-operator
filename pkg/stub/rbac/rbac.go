package rbac

import (
	"github.com/operator-framework/operator-sdk/pkg/sdk"
	"github.com/sirupsen/logrus"
	"gitlab.com/Cavegn/project-operator/pkg/apis/project/v1alpha1"
	"gitlab.com/Cavegn/project-operator/pkg/constants"
	corev1 "k8s.io/api/core/v1"
	"k8s.io/api/rbac/v1"
	metav1 "k8s.io/apimachinery/pkg/apis/meta/v1"
)

func CreateClusterRoleBindings(namespacedProject *v1alpha1.NamespacedProject, namespace corev1.Namespace) error {
	roleBindings, err := getRoleBindings(namespace.Name)
	if err != nil {
		return err
	}

	clusterrole2user := groupUsersByClusterRole(namespacedProject.Spec.Users)
	for clusterrole, users := range clusterrole2user {
		roleName, err := createOrUpdateClusterRoleBinding(namespace, users, clusterrole, false, roleBindings)
		if err == nil && roleName != "" {
			delete(roleBindings, roleName)
		}
	}
	clusterrole2sa := groupUsersByClusterRole(namespacedProject.Spec.ServiceAccounts)
	for clusterrole, sa := range clusterrole2sa {
		roleName, err := createOrUpdateClusterRoleBinding(namespace, sa, clusterrole, true, roleBindings)
		if err == nil && roleName != "" {
			delete(roleBindings, roleName)
		}
	}

	for _, roleBinding := range roleBindings {
		// existing / not configured roleBinding => delete if created by operator
		if controlledByOperator(roleBinding) {
			err := sdk.Delete(&roleBinding)
			if err != nil {
				logrus.Errorf("failed to delete roleBinding %s in namespace %s: %v",
					roleBinding.Name, namespace.Namespace, err)
				return err
			}
		}
	}
	return nil
}

// Returns true if RoleBinding is controlled by Operator
// (RoleBinding has Annotation constants.CREATED_BY_OPERATOR with value constants.CREATED_BY_OPERATOR_TRUE)
func controlledByOperator(secret v1.RoleBinding) bool {
	value, ok := secret.Annotations[constants.CREATED_BY_OPERATOR]
	return ok && value == constants.CREATED_BY_OPERATOR_TRUE
}

func groupUsersByClusterRole(users []v1alpha1.User) map[string][]string {
	clusterrole2user := make(map[string][]string)
	for _, user := range users {
		_, ok := clusterrole2user[user.ClusterRole]
		if ok {
			clusterrole2user[user.ClusterRole] = append(clusterrole2user[user.ClusterRole], user.Name)
		} else {
			clusterrole2user[user.ClusterRole] = []string{user.Name}
		}
	}
	return clusterrole2user
}

// Returns a Map containing all the RoleBindings of given Namespace
// Key = Name of the RoleBinding
// Value = RoleBinding
func getRoleBindings(namespace string) (map[string]v1.RoleBinding, error) {
	roleBindingList := roleBindingList()
	listOptions := &metav1.ListOptions{}
	err := sdk.List(namespace, roleBindingList, sdk.WithListOptions(listOptions))
	if err != nil {
		logrus.Errorf("failed to list rolebindings in namespace %s: %v", namespace, err)
		return nil, err
	}

	roleBindings := make(map[string]v1.RoleBinding)
	for _, secret := range roleBindingList.Items {
		roleBindings[secret.Name] = secret
	}

	return roleBindings, nil
}

func roleBindingList() *v1.RoleBindingList {
	return &v1.RoleBindingList{TypeMeta: metav1.TypeMeta{Kind: "RoleBinding", APIVersion: "rbac.authorization.k8s.io/v1"}}
}

func createOrUpdateClusterRoleBinding(namespace corev1.Namespace, users []string, clusterRole string,
	isServiceAccount bool, existingRoleBindings map[string]v1.RoleBinding) (string, error) {
	if len(users) == 0 {
		return "", nil
	}

	roleNamePrefix := "user"
	kind := "User"
	if isServiceAccount {
		roleNamePrefix = "sa"
		kind = "ServiceAccount"
	}

	roleName := roleNamePrefix + "-" + clusterRole

	roleBinding := v1.RoleBinding{
		TypeMeta: metav1.TypeMeta{Kind: "RoleBinding", APIVersion: "rbac.authorization.k8s.io/v1"},
		ObjectMeta: metav1.ObjectMeta{
			Name:      roleName,
			Namespace: namespace.Name,
			Annotations: map[string]string{
				constants.CREATED_BY_OPERATOR: constants.CREATED_BY_OPERATOR_TRUE,
			},
		},
		RoleRef: v1.RoleRef{Kind: "ClusterRole", Name: clusterRole},
	}

	for _, user := range users {
		subject := v1.Subject{
			Name: user,
			Kind: kind,
		}

		roleBinding.Subjects = append(roleBinding.Subjects, subject)
	}

	existingRolebinding, ok := existingRoleBindings[roleName]
	if ok {
		// RoleBinding already exists -> modify if allowed
		if controlledByOperator(existingRolebinding) {
			err := sdk.Update(&roleBinding)
			if err != nil {
				logrus.Errorf("failed to update rolebinding %s/%s: %v", namespace.Name, clusterRole, err)
				return "", err
			}
		}
	} else {
		// RoleBinding doesn't exist -> create
		err := sdk.Create(&roleBinding)
		if err != nil {
			logrus.Errorf("failed to create rolebinding %s/%s: %v", namespace.Name, clusterRole, err)
			return "", err
		}
	}

	return roleName, nil
}
