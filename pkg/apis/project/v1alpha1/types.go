package v1alpha1

import (
	metav1 "k8s.io/apimachinery/pkg/apis/meta/v1"
)

// +k8s:deepcopy-gen:interfaces=k8s.io/apimachinery/pkg/runtime.Object

type NamespacedProjectList struct {
	metav1.TypeMeta `json:",inline"`
	metav1.ListMeta `json:"metadata"`
	Items           []NamespacedProject `json:"items"`
}

// +k8s:deepcopy-gen:interfaces=k8s.io/apimachinery/pkg/runtime.Object

type NamespacedProject struct {
	metav1.TypeMeta   `json:",inline"`
	metav1.ObjectMeta `json:"metadata"`
	Spec              NamespacedProjectSpec   `json:"spec"`
	Status            NamespacedProjectStatus `json:"status,omitempty"`
}

type NamespacedProjectSpec struct {
	ProjectName      string `json: "projectName"`
	ProjectNamespace string `json: "projectNamespace"`
	ProjectOwner     string `json: "projectOwner"`

	PreserveSecrets []string `json: "preserveSecrets"`

	ServiceAccounts []User `json: "serviceAccounts"`
	Users           []User `json: "users"`
}

type User struct {
	Name        string `json: "name"`
	ClusterRole string `json: "clusterRole"`
}

type NamespacedProjectStatus struct {
	// Fill me
}
