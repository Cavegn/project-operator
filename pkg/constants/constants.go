package constants

const PROJECT_NAME = "project-name"
const PROJECT_OWNER = "project-owner"

const CREATED_BY_OPERATOR = "created-by-project-operator"
const CREATED_BY_OPERATOR_TRUE = "true"
